# Front-End Test assigment

1.Convert the following Figma design to HTML layout.

[Figma design](https://www.figma.com/file/Yi8CRLelsmFUmsOPwAA0Tr/Landing-Page-2?node-id=1%3A26)

Requirements:

- adaptive
- mobile menu could be a burger menu
- you can use any frameworks/libraries, CSS prepocessors, toolkits such Gulp or Webpack 
- the final project should have the following structure: 

```
 project-name
 - assets
 -- images
 -- css
 index.html
```

2. Code the curry function so it returns values like bellow:
```javascript
function abc(a, b, c) {
  return a + b + c;
}

function abcdef(a, b, c, d, e, f) {
  return a + b + c + d + e + f;
}

abc.curry('A')('B')('C'); // 'ABC'
abc.curry('A', 'B')('C'); // 'ABC'
abc.curry('A', 'B', 'C'); // 'ABC'

abcdef.curry('A')('B')('C')('D')('E')('F'); // 'ABCDEF'
abcdef.curry('A', 'B', 'C')('D', 'E', 'F'); // 'ABCDEF'
```

3.How this function can be improved? How would you refactor the `drawRating()` function if the function accept the `vote` parameter, that contains value from 0 to 100. It doesn't matter how stars look, just need to improve the logic. 
```javascript
function drawRating(vote) {
	if (vote >= 0 && vote <= 20) {
    	return '★☆☆☆☆';
	}
	else if (vote > 20 && vote <= 40) {
		return '★★☆☆☆';
	}
	else if (vote > 40 && vote <= 60) {
		return '★★★☆☆';
	}
	else if (vote > 60 && vote <= 80) {
		return '★★★★☆';
	}
	else if (vote > 80 && vote <= 100) {
		return '★★★★★';
	}
}

console.log(drawRating(0) ); // ★☆☆☆☆
console.log(drawRating(1) ); // ★☆☆☆☆
console.log(drawRating(50)); // ★★★☆☆
console.log(drawRating(99)); // ★★★★★
```
